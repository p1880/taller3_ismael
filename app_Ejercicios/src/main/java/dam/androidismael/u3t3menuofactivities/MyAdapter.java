package dam.androidismael.u3t3menuofactivities;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.androidismael.u3t3menuofactivities.model.Item;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private String[] myDataSet;
    private OnItemClickListener listener;

    public MyAdapter(String[] myDataSet, OnItemClickListener listener) {
        this.myDataSet = myDataSet;
        this.listener = listener;
    }

    static class MyViewHolder extends RecyclerView. ViewHolder {
        TextView textView;
        public MyViewHolder(TextView textView) {
            super (textView);
            this.textView = textView;
        }
        public void bind(String activityName, OnItemClickListener listener) {
            this.textView.setText(activityName);

            this.textView.setOnClickListener(v -> listener.onItemClick(textView.getText().toString()));
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView tv = (TextView) LayoutInflater.from(parent.getContext ())
                .inflate(android.R. layout.simple_list_item_1, parent, false);
        return new MyViewHolder(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind (myDataSet[position], listener);
    }

    @Override
    public int getItemCount() {
        return myDataSet.length;
    }
    public interface OnItemClickListener {
        void onItemClick(String activityName);
    }
}
