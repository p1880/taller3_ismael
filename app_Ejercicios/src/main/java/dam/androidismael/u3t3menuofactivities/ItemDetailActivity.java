package dam.androidismael.u3t3menuofactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class ItemDetailActivity extends AppCompatActivity {

    private TextView tv4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        setUI();
        escribirTv();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }
    private void setUI() {
        tv4 = findViewById(R.id.tv4);
    }
    private void escribirTv(){
        Bundle parametros =  this.getIntent().getExtras();
        String nombre = parametros.getString("nombre");
        tv4.setText(nombre);
        setTitle(nombre);
    }
    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(myIntent);
        return true;
    }
}