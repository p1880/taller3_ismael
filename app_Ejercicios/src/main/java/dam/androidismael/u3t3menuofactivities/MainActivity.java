package dam.androidismael.u3t3menuofactivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dam.androidismael.u3t3menuofactivities.model.Item;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private String[] myDataset = {"Activity 1", "Activity 2", "Activity 3"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        recyclerView = findViewById(R.id.recylerViewActivities);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new MyAdapter(myDataset, this);
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onItemClick(String activityName) {
        Intent i = new Intent(MainActivity.this, ItemDetailActivity.class);
        i.putExtra("nombre", activityName);
        Toast.makeText(this, activityName, Toast.LENGTH_LONG).show();
        startActivity(i);
    }
}