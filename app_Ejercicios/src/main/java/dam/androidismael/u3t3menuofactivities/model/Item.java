package dam.androidismael.u3t3menuofactivities.model;

public class Item {
    private int idImg;
    private String version;
    private String nombreVersion;
    private int anyo;
    private String nombreAPI;
    private String urlVersion;

    public Item() {
    }

    public Item(int idImg, String version, String nombreVersion, int anyo, String nombreAPI, String urlVersion) {
        this.idImg = idImg;
        this.version = version;
        this.nombreVersion = nombreVersion;
        this.anyo = anyo;
        this.nombreAPI = nombreAPI;
        this.urlVersion = urlVersion;
    }

    public int getIdImg() {
        return idImg;
    }

    public void setIdImg(int idImg) {
        this.idImg = idImg;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getNombreVersion() {
        return nombreVersion;
    }

    public void setNombreVersion(String nombreVersion) {
        this.nombreVersion = nombreVersion;
    }

    public int getAnyo() {
        return anyo;
    }

    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }

    public String getNombreAPI() {
        return nombreAPI;
    }

    public void setNombreAPI(String nombreAPI) {
        this.nombreAPI = nombreAPI;
    }

    public String getUrlVersion() {
        return urlVersion;
    }

    public void setUrlVersion(String urlVersion) {
        this.urlVersion = urlVersion;
    }
}
