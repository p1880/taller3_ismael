package dam.androidismael.u3t3menuofactivities;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import dam.androidismael.u3t3menuofactivities.model.Item;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<Item>mData;
    private LayoutInflater mInflater;
    private Context context;
    private OnItemClickListener listener;

    public MyAdapter(Context context, List<Item> items, OnItemClickListener listener) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.mData = items;
        this.listener = listener;
    }

    @Override
    public int getItemCount(){return mData.size();}

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view= mInflater.inflate(R.layout.pruebalayout2, null);
        return new MyAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyAdapter.ViewHolder holder, final int position){
        holder.bindData(mData.get(position));
        holder.bind (mData, position,listener);
    }

    public void setItems(List<Item> items){mData = items;}


    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView iconImage;
        TextView version, nombreVersion, nombreAPI;
        CardView cardView;


        ViewHolder(View itemView){
            super(itemView);
            iconImage = itemView.findViewById(R.id.iconImageView);
            version = itemView.findViewById(R.id.version);
            nombreVersion = itemView.findViewById(R.id.nombreVersion);
            nombreAPI = itemView.findViewById(R.id.nombreAPI);
            cardView = itemView.findViewById(R.id.cv);
        }

        void bindData(final Item item){
            iconImage.setImageResource(item.getId());
            version.setText("Version: " + item.getVersion());
            nombreVersion.setText(item.getNombreVersion());
            nombreAPI.setText("API: " + item.getNombreAPI());
        }
        public void bind(List<Item> item, int posicion, OnItemClickListener listener) {
            this.cardView.setOnClickListener(v -> listener.onItemClick(item, posicion));
        }
    }
    public interface OnItemClickListener {
        void onItemClick(List<Item> item, int posicion);
    }
}
