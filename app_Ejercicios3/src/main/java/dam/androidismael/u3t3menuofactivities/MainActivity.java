package dam.androidismael.u3t3menuofactivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import dam.androidismael.u3t3menuofactivities.model.Item;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener{
    List<Item> items;
    private MyAdapter myAdapter;
    RecyclerView recyclerView;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        items = new ArrayList<>();
        items.add(new Item(R.drawable.android12,"12.0","Android 12","2021","31","https://es.wikipedia.org/wiki/Android_12"));
        items.add(new Item(R.drawable.android11,"11.0","Android 11","2020","30","https://es.wikipedia.org/wiki/Android_11"));
        items.add(new Item(R.drawable.android10,"10.0","Android 10","2019","29","https://es.wikipedia.org/wiki/Android_10"));
        items.add(new Item(R.drawable.pie,"9.0","Pie","2018","28","https://es.wikipedia.org/wiki/Android_Pie"));
        items.add(new Item(R.drawable.oreo,"8.0","Oreo","2017","26","https://es.wikipedia.org/wiki/Android_Oreo"));
        items.add(new Item(R.drawable.naugat,"7.0","Nougat","2016","24","https://es.wikipedia.org/wiki/Android_Nougat"));
        items.add(new Item(R.drawable.mashmallow,"6.0","Marshmallow","2015","23","https://es.wikipedia.org/wiki/Android_Marshmallow"));
        items.add(new Item(R.drawable.lollipop,"5.0","Lollipop","2014","21","https://es.wikipedia.org/wiki/Android_Lollipop"));

        myAdapter = new MyAdapter(this, items, this);
        recyclerView = findViewById(R.id.recyclerItem);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        new ItemTouchHelper(iTHC).attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(myAdapter);
        img = findViewById(R.id.img);
    }

    public void onItemClick(List<Item> item, int posicion) {
        Intent i = new Intent(MainActivity.this, ItemDetailActivity.class);
        i.putExtra("nombre", item.get(posicion).getNombreVersion());
        i.putExtra("version", item.get(posicion).getVersion());
        i.putExtra("anyo", item.get(posicion).getAnyo());
        i.putExtra("nombreAPI", item.get(posicion).getNombreAPI());
        i.putExtra("url", item.get(posicion).getUrlVersion());
        i.putExtra("img", item.get(posicion).getId());
        startActivity(i);
    }
    public void botones(View v){
        switch (v.getId()){
            case R.id.btadd:
                items.add(new Item(R.drawable.lollipop,"5.0","Lollipop","2014","21","https://es.wikipedia.org/wiki/Android_Lollipop"));
                myAdapter = new MyAdapter(this, items, this);
                recyclerView = findViewById(R.id.recyclerItem);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.setAdapter(myAdapter);
                img.setVisibility(View.INVISIBLE);
                break;
            case R.id.btDelete:
                items.clear();
                myAdapter = new MyAdapter(this, items, this);
                recyclerView = findViewById(R.id.recyclerItem);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.setAdapter(myAdapter);
                img.setVisibility(View.VISIBLE);
                break;
            case R.id.btRestore:
                setUI();
                img.setVisibility(View.INVISIBLE);
                break;
        }
    }
    ItemTouchHelper.SimpleCallback iTHC = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            items.remove(viewHolder.getAdapterPosition());
            myAdapter.notifyDataSetChanged();
            if(items.size() == 0){
                img.setVisibility(View.VISIBLE);
            }
        }
    };
}