package dam.androidismael.u3t3menuofactivities.model;

public class Item {
    private int id;
    private String version;
    private String nombreVersion;
    private String anyo;
    private String nombreAPI;
    private String urlVersion;

    public Item() {
    }

    public Item(int id, String version, String nombreVersion, String anyo, String nombreAPI, String urlVersion) {
        this.id = id;
        this.version = version;
        this.nombreVersion = nombreVersion;
        this.anyo = anyo;
        this.nombreAPI = nombreAPI;
        this.urlVersion = urlVersion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getNombreVersion() {
        return nombreVersion;
    }

    public void setNombreVersion(String nombreVersion) {
        this.nombreVersion = nombreVersion;
    }

    public String getAnyo() {
        return anyo;
    }

    public void setAnyo(String anyo) {
        this.anyo = anyo;
    }

    public String getNombreAPI() {
        return nombreAPI;
    }

    public void setNombreAPI(String nombreAPI) {
        this.nombreAPI = nombreAPI;
    }

    public String getUrlVersion() {
        return urlVersion;
    }

    public void setUrlVersion(String urlVersion) {
        this.urlVersion = urlVersion;
    }
}
