package dam.androidismael.u3t3menuofactivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActionBar;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemDetailActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String IMPLICIT_INTENTS = "ImplicitIntent";
    ImageButton iconImage;
    TextView version, nombreVersion, anyo, nombreAPI, urlVersion;
    String versionS, nombreVersionS, anyoS, nombreAPIS, urlVersionS;
    int img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        setUI();
        escribirTv();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    private void setUI() {
        iconImage = findViewById(R.id.btimg);
        version = findViewById(R.id.version);
        nombreVersion = findViewById(R.id.nombre);
        anyo = findViewById(R.id.anyo);
        nombreAPI = findViewById(R.id.api);

    }
    private void escribirTv(){
        Bundle parametros =  this.getIntent().getExtras();
        nombreVersionS = parametros.getString("nombre");
        versionS = parametros.getString("version");
        anyoS = parametros.getString("anyo");
        nombreAPIS = parametros.getString("nombreAPI");
        urlVersionS = parametros.getString("url");
        img = parametros.getInt("img");


        setTitle(nombreVersionS);
        nombreVersion.setText(nombreVersionS);
        version.setText("Version: " + versionS);
        anyo.setText(anyoS);
        nombreAPI.setText("API: " + nombreAPIS);
        iconImage.setBackgroundResource(img);
    }
    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(myIntent);
        return true;
    }

    private void openWebsite(String urlText){
        Uri webpage = Uri.parse(urlText);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

        if(intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }else{
            Log.d(IMPLICIT_INTENTS, "openWebsite: Can't handle this intent!");
        }
    }
    public void onClick(View view) {
        openWebsite(urlVersionS);
    }
}
